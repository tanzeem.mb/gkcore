{
    "$schema": "https://json-schema.org/draft/2019-09/schema",
    "$id": "http://example.com/example.json",
    "type": "object",
    "default": {},
    "title": "Root Schema",
    "required": [
        "gkstatus",
        "gkresult"
    ],
    "properties": {
        "gkstatus": {
            "type": "integer",
            "default": 0,
            "title": "The gkstatus Schema",
            "examples": [
                0
            ]
        },
        "gkresult": {
            "type": "object",
            "default": {},
            "title": "The gkresult Schema",
            "required": [
                "userquestion",
                "userid"
            ],
            "properties": {
                "userquestion": {
                    "type": "string",
                    "default": "",
                    "title": "The userquestion Schema",
                    "examples": [
                        "Your Lucky number ?"
                    ]
                },
                "userid": {
                    "type": "integer",
                    "default": 0,
                    "title": "The userid Schema",
                    "examples": [
                        1
                    ]
                }
            },
            "examples": [{
                "userquestion": "Your Lucky number ?",
                "userid": 1
            }]
        }
    },
    "examples": [{
        "gkstatus": 0,
        "gkresult": {
            "userquestion": "Your Lucky number ?",
            "userid": 1
        }
    }]
}